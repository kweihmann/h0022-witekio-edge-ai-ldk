# Copyright (C) 2020 AVNET Integrated, MSC Technologies GmbH
# @TYPE: Machine
# @NAME: sm2s-imx8mp module
# @SOC: i.MX8MP
# @DESCRIPTION: Machine configuration for sm2s-imx8qm
# derived from meta-fsl-bsp-releases/meta-bsp/conf/machine/imx8mpevk.conf

MACHINEOVERRIDES =. "mx8:mx8m:mx8mp:"

require conf/machine/include/imx-base.inc
require conf/machine/include/imx-preferred-env.inc
require conf/machine/include/tune-cortexa53.inc

IMAGE_FSTYPES += "wic wic.bmap"

MACHINE_FEATURES_remove = "nxp8987"

KERNEL_DEVICETREE = " \
		msc/imx8mp/msc-sm2s-imx8mp-001-headless.dtb \
		msc/imx8mp/msc-sm2s-imx8mp-001-hdmi.dtb \
		msc/imx8mp/msc-sm2s-imx8mp-001-dsi.dtb \
		msc/imx8mp/msc-sm2s-imx8mp-001-twin-cam-dsi.dtb \
		msc/imx8mp/msc-sm2s-imx8mp-001-twin-cam-hdmi.dtb \
		\
		msc/imx8mp/msc-sm2s-imx8mp-002-headless.dtb \
		msc/imx8mp/msc-sm2s-imx8mp-002-hdmi.dtb \
		msc/imx8mp/msc-sm2s-imx8mp-002-lvds0-native-single-mode.dtb \
		msc/imx8mp/msc-sm2s-imx8mp-002-lvds0-native-dual-mode.dtb \
		msc/imx8mp/msc-sm2s-imx8mp-002-twin-cam-lvds0.dtb \
		msc/imx8mp/msc-sm2s-imx8mp-002-twin-cam-hdmi.dtb \
		\
		msc/imx8mp/msc-sm2s-imx8mp-003-headless.dtb \
		msc/imx8mp/msc-sm2s-imx8mp-003-hdmi.dtb \
		msc/imx8mp/msc-sm2s-imx8mp-003-lvds0-native-single-mode.dtb \
		msc/imx8mp/msc-sm2s-imx8mp-003-lvds1-bridged-single-mode.dtb \
		msc/imx8mp/msc-sm2s-imx8mp-003-twin-lvds.dtb \
		msc/imx8mp/msc-sm2s-imx8mp-003-hdmi-twin-lvds.dtb \
		"

UBOOT_CONFIG ??= "sd"
UBOOT_CONFIG[sd] = "msc_sm2s_imx8mp_defconfig,sdcard"
SPL_BINARY = "spl/u-boot-spl.bin"

# Set u-boot DTB
UBOOT_DTB_NAME = "msc-sm2s-imx8mp.dtb"

# Set DDR FIRMWARE
DDR_FIRMWARE_NAME = " \
		lpddr4_pmu_train_1d_dmem_201904.bin \
		lpddr4_pmu_train_1d_imem_201904.bin \
		lpddr4_pmu_train_2d_dmem_201904.bin \
		lpddr4_pmu_train_2d_imem_201904.bin \
		"

# Set imx-mkimage boot target
IMXBOOT_TARGETS = " \
		flash_hdmi_spl_uboot \
		"

# Set Serial console
SERIAL_CONSOLES = "115200;ttymxc1"

IMAGE_BOOTLOADER = "imx-boot"

LOADADDR = ""
UBOOT_SUFFIX = "bin"
UBOOT_MAKE_TARGET = ""
IMX_BOOT_SEEK = "32"

OPTEE_BIN_EXT = "8mp"

IMAGE_INSTALL_append += " \
		${@bb.utils.contains('DISTRO_FEATURES', 'wayland', \
			'weston weston-init weston-examples', '', d)} \
		devmem2 memtester evtest fb-test \
		python-pyserial \
		imx-boot \
		test-suite \
		"
