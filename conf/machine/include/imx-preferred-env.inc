# derived from fsl-imx-preferred-env.inc

# Use i.MX Kernel, U-Boot and Gstreamer 1.0 providers
PREFERRED_PROVIDER_virtual/bootloader_imx = "u-boot-imx"
PREFERRED_PROVIDER_virtual/kernel_imx = "linux-imx"
PREFERRED_VERSION_linux-libc-headers_imx ?= "5.4"
MACHINE_USES_VIVANTE_KERNEL_DRIVER_MODULE = "1"

# Extra audio support
# Add support for ALL i.MX6/7/8 SoC families
#MACHINE_EXTRA_RRECOMMENDS_append_imx = " ${@bb.utils.contains('DISTRO_FEATURES', 'alsa', 'imx-alsa-plugins', '', d)}"

# Default toolchains used in testing i.MX BSPs
#DEFAULTTUNE_mx8 = "aarch64"

# Enable the kenrel loadable module as default
#USE_GPU_VIV_MODULE = "1"

IMX_GPU_VERSION            ?= "6.4.3.p0.0"
IMX_GPU_VERSION_SUFFIX      = "aarch32"
IMX_GPU_VERSION_SUFFIX_mx8  = "aarch64"

PREFERRED_VERSION_imx-gpu-viv               = "${IMX_GPU_VERSION}-${IMX_GPU_VERSION_SUFFIX}"
PREFERRED_VERSION_kernel-module-imx-gpu-viv = "${IMX_GPU_VERSION}"
PREFERRED_VERSION_imx-gpu-g2d               = "${IMX_GPU_VERSION}"

# Set the preffered provider for opencl-headers
PREFERRED_PROVIDER_opencl-headers_imx = "imx-gpu-viv"

PREFERRED_VERSION_gstreamer1.0-plugins-base = "1.16.1.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-bad  = "1.16.1.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-good = "1.16.1.imx"
PREFERRED_VERSION_gstreamer1.0              = "1.16.1.imx"
PREFERRED_VERSION_gstreamer1.0-libav        = "1.16.1"
