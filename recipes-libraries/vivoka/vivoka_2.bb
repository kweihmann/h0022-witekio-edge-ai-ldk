DESCRIPTION = "Vivoka SDK"
#LICENSE = "Apache-2.0"
#LIC_FILES_CHKSUM = "file://LICENSE;md5=64a34301f8e355f57ec992c2af3e5157"
LICENSE = "CLOSED"
SRC_URI += "file://csdk_v${PV}.tar.gz"

do_install() {
    cd ${S}
    
    # install libraries
    install -d ${D}/${libdir}/vivoka
    for lib in ${S}/../lib/*
    do
        install -m 0555 $lib ${D}${libdir}/vivoka
    done

    # install header files
    install -d ${D}/${includedir}/vivoka
    cd ${S}/..
    cp -r inc/* ${D}${includedir}/vivoka
}

#SYSROOT_PREPROCESS_FUNCS += "rpm_sysroot_preprocess"

do_package_qa[noexec] = "1"

rpm_sysroot_preprocess () {
   # install libraries
    install -d ${SYSROOT_DESTDIR}/${libdir}/vivoka
    for lib in ${S}/../lib/*
    do
        install -m 0555 $lib ${SYSROOT_DESTDIR}/${libdir}/vivoka
    done

    # install header files
    install -d ${SYSROOT_DESTDIR}/${includedir}/vivoka/inc
    cd ${S}/..
    cp -r inc/* ${SYSROOT_DESTDIR}/${includedir}/vivoka/inc
}


