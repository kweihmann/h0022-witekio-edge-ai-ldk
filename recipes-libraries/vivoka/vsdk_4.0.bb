DESCRIPTION = "Vivoka VSDK"
#LICENSE = "Apache-2.0"
#LIC_FILES_CHKSUM = "file://LICENSE;md5=64a34301f8e355f57ec992c2af3e5157"
LICENSE = "CLOSED"
SRC_URI += "file://vsdk_v${PV}.tar.gz"

do_install() {
    cd ${S}
    
    # install libraries
    install -d ${D}/${libdir}/vsdk
    for lib in ${S}/../lib/*
    do
        install -m 0555 $lib ${D}${libdir}/vsdk
    done

    # install header files
    install -d ${D}/${includedir}/vsdk
    cd ${S}/..
    cp -r include/vsdk/* ${D}${includedir}/vsdk
}

#SYSROOT_PREPROCESS_FUNCS += "rpm_sysroot_preprocess"

do_package_qa[noexec] = "1"

rpm_sysroot_preprocess () {
   # install libraries
    install -d ${SYSROOT_DESTDIR}/${libdir}/vsdk
    for lib in ${S}/../lib/*
    do
        install -m 0555 $lib ${SYSROOT_DESTDIR}/${libdir}/vsdk
    done

    # install header files
    install -d ${SYSROOT_DESTDIR}/${includedir}/vsdk
    cd ${S}/..
    cp -r include/vsdk/* ${SYSROOT_DESTDIR}/${includedir}/vsdk
}


