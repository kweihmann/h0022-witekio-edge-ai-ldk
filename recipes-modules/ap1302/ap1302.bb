SUMMARY = "Recipe for building an external ap1302 Linux kernel module"
SECTION = "PETALINUX/modules"

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"

DEPENDS += "virtual/kernel"

#SRC_URI = "git://github.com/Avnet/ap1302-driver.git;protocol=https;branch=master"

#SRC_URI += "file://0001-support-providing-fw-name-in-device-tree.patch"
#SRC_URI += "file://0001-sync-with-xilinx-src.patch"
#SRC_URI += "file://0001-fix-sensor-unregister.patch"

SRC_URI = "git://github.com/sbertrand-witekio/ap1302-driver.git;protocol=https;branch=imx8mplus/dev"

SRCREV = "ed80bfd73fe9cee988aadf6384a130ee63f65b73"

S = "${WORKDIR}/git"

inherit module

EXTRA_OEMAKE = 'CONFIG_VIDEO_AP1302=m \
		CONFIG_CMA_SIZE_MBYTES=1024 \
		KERNEL_SRC="${STAGING_KERNEL_DIR}" \
		O=${STAGING_KERNEL_BUILDDIR}'

