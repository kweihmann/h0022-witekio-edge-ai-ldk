/*
 * Copyright (C) 2021 AVNET Integrated, MSC Technologies GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/dts-v1/;
/plugin/;

#include <dt-bindings/gpio/gpio.h>

/ {
	fragment@7000 {
		target-path="/";
		__overlay__ {
			ap1302_osc: ap1302oscillator {
				compatible = "fixed-clock";
				#clock-cells = <0>;
				clock-frequency  = <48000000>;
				clock-output-names = "ap1302osc";
			};
		};
	};

	fragment@7001 {
		target = <&i2c5>;
		__overlay__ {
			clock-frequency = <400000>;
			
			ap1302_camera@3c {
				compatible = "onnn,ap1302";
				reg = <0x3c>;
				clocks = <&ap1302_osc>;
				/*
					pinmux pinctrl_smarc_gpio done by hog
					SMARC GPIO3 aka SAI1_RXD1
					SMARC GPIO1 aka SAI1_RXC
				*/
				
				reset-gpios = <&gpio4 3 GPIO_ACTIVE_LOW>;
				standby-gpios = <&gpio4 1 GPIO_ACTIVE_LOW>;

				fw-name = "smarc_ap1302_ar1335_dual_fw.bin";

				status = "okay";
		
				port@2 {
					reg = <2>;
					ap1302_out_ep: endpoint {
						remote-endpoint = <&mipi_csi1_ep>;
						clock-lanes = <0>;
						data-lanes = <1 2 3 4>;
					};
				};
		
				sensors {
					#address-cells = <1>;
					#size-cells = <0>;
		
					onnn,model = "onnn,ar1335";
		
					sensor@0 {
						reg = <0>;
					};
					
					sensor@1 {
						reg = <1>;
					};
				};
			};
		};
	};

	fragment@7002 {
		target = <&mipi_csi_1>;
		__overlay__ {
			
			clock-frequency = <266000000>;
			assigned-clock-rates = <266000000>;
			status = "okay";
			port@1 {
				reg = <1>;
				mipi_csi1_ep: endpoint {
					remote-endpoint = <&ap1302_out_ep>;
					data-lanes = <4>;
					csis-hs-settle = <16>; /* may need to be 16 ? */
					csis-clk-settle = <2>;
					csis-wclk;
				};
			};
		};
	};

	fragment@7003 {
		target = <&cameradev>;
		__overlay__ {
			status = "okay";
		};
	};

	fragment@7004{
		target = <&isi_1>;
		__overlay__ {
			status = "okay";

			cap_device {
				status = "okay";
			};
		};
	};

	/* why is mipi dsi enabled ?*/
	fragment@7005 {
		target = <&mipi_dsi>;
		__overlay__ {
			status = "okay";
		};
	};
};

