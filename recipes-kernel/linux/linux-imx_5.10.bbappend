FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

# disable SCMVERSION
SCMVERSION = "n"

SRC_URI += "file://imx8mp-evk-ap1302-csi1.dts"
SRC_URI += "file://imx8mp-evk-ap1302-csi2.dts"
SRC_URI += "file://overlay-cam1-ap1302.dts"
SRC_URI += "file://0002-media-imx-imx8-fixes-for-compatibility-with-ap1302.patch"
SRC_URI += "file://0004-media-imx-input-ioctl-for-imx8-isi-capture.patch"
SRC_URI += "file://0005-media-imx-imx8-media-device-remains-registered-with-.patch"
SRC_URI += "file://imx8-media-dev.cfg"

do_configure_prepend_imx8mpevk() {
	cp ${WORKDIR}/imx8mp-evk-ap1302-csi1.dts ${S}/arch/arm64/boot/dts/freescale/imx8mp-evk-ap1302-csi1.dts
	cp ${WORKDIR}/imx8mp-evk-ap1302-csi2.dts ${S}/arch/arm64/boot/dts/freescale/imx8mp-evk-ap1302-csi2.dts
}

do_configure_prepend_sm2s-imx8mp() {
	cp ${WORKDIR}/overlay-cam1-ap1302.dts ${S}/arch/arm64/boot/dts/msc/imx8mp/overlay-cam1-ap1302.dts
}

require recipes-kernel/linux/kernel-device-tree-ap1302.inc

