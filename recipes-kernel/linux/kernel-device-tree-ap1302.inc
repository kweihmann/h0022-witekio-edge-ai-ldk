# Copyright (C) 2019 Witekio
# Released under the MIT license (see COPYING.MIT for the terms)

# Provide device tree to support AP1302 on platforms
#
# used in the kernel recipe and the image recipe
# when only references in kernel recipe
# device tree missing in boot image

KERNEL_DEVICETREE_append_imx8mpevk = "freescale/imx8mp-evk-ap1302-csi1.dtb freescale/imx8mp-evk-ap1302-csi2.dtb"
KERNEL_DEVICETREE_append_sm2s-imx8mp = "msc/imx8mp/overlay-cam1-ap1302.dtb"
