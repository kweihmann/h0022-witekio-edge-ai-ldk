# Add ap1302 and v4l utils (v4l2-ctl and media-ctl)
IMAGE_INSTALL_append += " \
	ap1302 \
	ap1302-firmware \
	media-ctl \
	v4l-utils \
"

# Add qt multimedia camera example
IMAGE_INSTALL_append += " \
	qtmultimedia-examples \
	"

require recipes-kernel/linux/kernel-device-tree-ap1302.inc
