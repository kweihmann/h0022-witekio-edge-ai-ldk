# Copyright (C) 2020 Witekio
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-fsl/images/imx-image-multimedia.bb

inherit populate_sdk_qt5

CONFLICT_DISTRO_FEATURES = "directfb"

do_rootfs[depends] += "virtual/bootloader:do_deploy virtual/kernel:do_deploy"

IMAGE_INSTALL += " \
    rsync \
    openssh-sftp-server \
"

#IMAGE_INSTALL += " packagegroup-qt5-imx "

FEATURES_TO_REMOVE = " \
	package-management \
	splash \
	tools-profile \
    tools-sdk \
    nfs-server \
    tools-testapps \
    hwcodecs \
"

IMAGE_FEATURES_remove ?= "${FEATURES_TO_REMOVE}"

CORE_IMAGE_EXTRA_INSTALL_remove = " \
    packagegroup-tools-bluetooth \
    packagegroup-fsl-tools-audio \
    packagegroup-fsl-tools-gpu \
    packagegroup-fsl-tools-gpu-external \
    packagegroup-fsl-tools-testapps \
    packagegroup-fsl-tools-benchmark \
    packagegroup-imx-security \
"

# Add ap1302 and v4l utils (v4l2-ctl and media-ctl)
IMAGE_INSTALL_append += " \
	ap1302 \
	ap1302-firmware \
	media-ctl \
	v4l-utils \
	htop \
	ap1302-gstreamer-demo \
    udev-extraconf \
    minicom \
    mmc-utils \
    nano \
    e2fsprogs-resize2fs \
    coreutils \
    cpufrequtils \
    procps \
    screen \
    connman-client \
    connman-info \
    eiq-apps \
"

# Add qt multimedia camera example
IMAGE_INSTALL_append = " \
    qtmultimedia-examples \
    qtmultimedia-plugins \
    qtwayland \
    cinematicexperience-rhi \
"

require recipes-kernel/linux/kernel-device-tree-ap1302.inc

