# Copyright (C) 2021 Witekio
# Released under the MIT license (see COPYING.MIT for the terms)
DESCRIPTION = "AP1302 GStreamer Demo"
LICENSE = "GPLv3.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/LGPL-3.0-only;md5=bfccfe952269fff2b407dd11f2f3083b"

SRC_URI = "file://ap1302-gstreamer-demo.initd \
"

inherit systemd update-rc.d
INITSCRIPT_NAME = "ap1302-gstreamer-demo"
INITSCRIPT_PARAMS = "start 99 5 2 . stop 20 0 ."


do_install() {
	install -Dm 0755 ${WORKDIR}/ap1302-gstreamer-demo.initd ${D}${sysconfdir}/init.d/ap1302-gstreamer-demo
}

FILES_${PN} += "${datadir}"

RDEPENDS_${PN} = "ap1302 \
           ap1302-firmware \
           gstreamer1.0 \
           "
