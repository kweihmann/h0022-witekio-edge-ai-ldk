# Copyright (C) 2019 Witekio
# Released under the MIT license (see COPYING.MIT for the terms)
DESCRIPTION = "TensorFlow Lite and Qt/QML: Connected Crane demo"
LICENSE = "GPLv3.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/LGPL-3.0-only;md5=bfccfe952269fff2b407dd11f2f3083b"

S = "${WORKDIR}/git"
SRC_URI = "gitsm://git@bitbucket.org/adeneo-embedded/b2220-avnet-edge-ai-startup-app.git;branch=dev;protocol=ssh \
	   file://setup.initd \
"
SRCREV = "${AUTOREV}"

DEPENDS = "qtquickcontrols2 \
           qtmultimedia \
           qtconnectivity \
           qttools-native \
           portaudio-v19 \
           "

require recipes-qt/qt5/qt5.inc

inherit systemd update-rc.d
INITSCRIPT_NAME = "setup"
INITSCRIPT_PARAMS = "start 99 5 2 . stop 20 0 ."

CXXFLAGS_remove = "-O2"

do_install() {
	install -d ${D}${datadir}/${P}
	install -m 0755 ${B}/setup ${D}${datadir}/${P}/setup
	
	install -Dm 0755 ${WORKDIR}/setup.initd ${D}${sysconfdir}/init.d/setup
}

FILES_${PN}-dbg += "${datadir}/${P}/.debug"
FILES_${PN} += "${datadir}"

RDEPENDS_${PN} = "qtquickcontrols2-qmlplugins \
                  qtgraphicaleffects-qmlplugins \
		 qtconnectivity"
