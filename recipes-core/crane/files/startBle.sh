#!/bin/sh

modprobe btusb
modprobe hci_uart
modprobe rfcomm
modprobe hidp
modprobe btbcm
modprobe btqca

/etc/init.d/bluetooth restart

hciconfig hci0 up
hciconfig hci0 noscan
hciconfig hci0 auth
hciconfig hci0 down
hciconfig hci0 up

