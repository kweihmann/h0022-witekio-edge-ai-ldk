# Copyright (C) 2019 Witekio
# Released under the MIT license (see COPYING.MIT for the terms)
DESCRIPTION = "TensorFlow Lite and Qt/QML: Connected Crane demo"
LICENSE = "GPLv3.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/LGPL-3.0-only;md5=bfccfe952269fff2b407dd11f2f3083b"

S = "${WORKDIR}/git"
SRC_URI = "gitsm://git@bitbucket.org/adeneo-embedded/b2220-avnet-edge-ai-cranedemo.git;branch=vsdk;protocol=ssh \
	   file://crane.initd \
           file://startBle.sh \
"
SRCREV = "${AUTOREV}"

DEPENDS = "qtquickcontrols2 \
           qtmultimedia \
	   qtwebsockets \
           qtconnectivity \
           qttools-native \
           tensorflow-lite \
           "

require recipes-qt/qt5/qt5.inc

CXXFLAGS_remove = "-O2"

do_install() {
	install -d ${D}${datadir}/${P}
	install -m 0755 ${B}/ConnectedCrane ${D}${datadir}/${P}/crane
	cp -R --no-dereference --preserve=mode,links ${B}/assets ${D}${datadir}/${P}
	install -m 0755 ${WORKDIR}/startBle.sh ${D}${datadir}/${P}/startBle.sh

    install -Dm 0755 ${WORKDIR}/crane.initd ${D}${sysconfdir}/init.d/crane

}

FILES_${PN}-dbg += "${datadir}/${P}/.debug"
FILES_${PN} += "${datadir}"

RDEPENDS_${PN} = "qtquickcontrols2-qmlplugins \
                  qtgraphicaleffects-qmlplugins \
		 qtconnectivity"
