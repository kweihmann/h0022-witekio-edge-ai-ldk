# Copyright (C) 2019 Witekio
# Released under the MIT license (see COPYING.MIT for the terms)
DESCRIPTION = "TensorFlow Lite and Qt/QML: Oven demo"
LICENSE = "GPLv3.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/LGPL-3.0-only;md5=bfccfe952269fff2b407dd11f2f3083b"

S = "${WORKDIR}/git"
SRC_URI = "gitsm://git@bitbucket.org/adeneo-embedded/b2220-avnet-edge-ai-oven.git;branch=demo-kit-stop;protocol=ssh \
	   file://oven.initd \           
"
SRCREV = "${AUTOREV}"

DEPENDS = "qtquickcontrols2 \
           qtmultimedia \
	       qtwebsockets \
           qtconnectivity \
           qttools-native \
           tensorflow-lite \
           "

require recipes-qt/qt5/qt5.inc

inherit systemd update-rc.d
INITSCRIPT_NAME = "oven"
INITSCRIPT_PARAMS = "start 99 5 2 . stop 20 0 ."

CXXFLAGS_remove = "-O2"

do_install() {
	install -d ${D}${datadir}/${P}
	install -m 0755 ${B}/main ${D}${datadir}/${P}/oven

    install -Dm 0755 ${WORKDIR}/oven.initd ${D}${sysconfdir}/init.d/oven

}

FILES_${PN}-dbg += "${datadir}/${P}/.debug"
FILES_${PN} += "${datadir}"

RDEPENDS_${PN} = "qtquickcontrols2-qmlplugins \
                  qtgraphicaleffects-qmlplugins \
		 qtconnectivity"
