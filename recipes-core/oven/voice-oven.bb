# Copyright (C) 2019 Witekio
# Released under the MIT license (see COPYING.MIT for the terms)
DESCRIPTION = "Voice command application"
#LICENSE = "CLOSED"
LICENSE = "GPLv3.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/LGPL-3.0-only;md5=bfccfe952269fff2b407dd11f2f3083b"

S = "${WORKDIR}/git"
SRC_URI = "gitsm://git@bitbucket.org/adeneo-embedded/b2220-avnet-edge-ai-voice-oven.git;branch=develop-vdk;protocol=ssh \
	   file://voice_oven.initd \
"
SRCREV = "${AUTOREV}"

DEPENDS = "libwebsockets \
           alsa-lib \
	       vsdk \
	       portaudio-v19 \
	       boost \
	       cjson \
	       nlohmann-json \
	       fmt \
           "
           
inherit pkgconfig cmake

inherit systemd update-rc.d
INITSCRIPT_NAME = "voice_oven"
INITSCRIPT_PARAMS = "start 99 5 2 . stop 20 0 ."

EXTRA_OECMAKE += "-DYOCTO_BUILD=1"
EXTRA_OECMAKE += "-DVSDK_INC=${STAGING_DIR_TARGET}${includedir}/vsdk"
EXTRA_OECMAKE += "-DVSDK_LIB=${STAGING_DIR_TARGET}${libdir}/vsdk"

do_install() {
	install -d ${D}${datadir}/${P}
	install -m 0755 ${B}/voice ${D}${datadir}/${P}/voice_oven

#Conpy config files
	cp -R --no-dereference --preserve=mode,links ${WORKDIR}/git/config ${D}${datadir}/${P}

	install -Dm 0755 ${WORKDIR}/voice_oven.initd ${D}${sysconfdir}/init.d/voice_oven
#Copy data files
	cp -R --no-dereference --preserve=mode,links ${WORKDIR}/git/data ${D}${datadir}/${P}
}

do_package_qa[noexec] = "1"

FILES_${PN}-dbg += "${datadir}/${P}/.debug"
FILES_${PN} += "${datadir}"

do_compile[depends] += "vsdk:do_populate_sysroot \
			portaudio-v19:do_populate_sysroot \
			boost:do_populate_sysroot \
                        cjson:do_populate_sysroot \
                        fmt:do_populate_sysroot \     
                        libwebsockets:do_populate_sysroot \
                        alsa-lib:do_populate_sysroot \
                        nlohmann-json:do_populate_sysroot \
                        fmt:do_populate_sysroot"
