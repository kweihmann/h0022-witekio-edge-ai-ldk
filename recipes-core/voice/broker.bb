# Copyright (C) 2019 Witekio
# Released under the MIT license (see COPYING.MIT for the terms)
DESCRIPTION = "TensorFlow Lite and Qt/QML: Connected Crane demo"
LICENSE = "GPLv3.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/LGPL-3.0-only;md5=bfccfe952269fff2b407dd11f2f3083b"

S = "${WORKDIR}/git"
SRC_URI = "gitsm://git@bitbucket.org/adeneo-embedded/b2220-avnet-edge-ai-broker.git;branch=master;protocol=ssh \
	   file://broker.initd \
"
SRCREV = "${AUTOREV}"

DEPENDS = "libwebsockets \
           "
inherit systemd update-rc.d

INITSCRIPT_NAME = "broker"
INITSCRIPT_PARAMS = "start 99 5 2 . stop 20 0 ."

EXTRA_OEMAKE += "CFLAGS+=-I${STAGING_DIR_TARGET}${includedir}"
EXTRA_OEMAKE += "CFLAGS+=-L${STAGING_DIR_TARGET}${libdir}"

do_install() {
	install -d ${D}${datadir}/${P}
	install -m 0755 ${B}/WebSocketServer ${D}${datadir}/${P}/broker

    install -Dm 0755 ${WORKDIR}/broker.initd ${D}${sysconfdir}/init.d/broker

}

FILES_${PN}-dbg += "${datadir}/${P}/.debug"
FILES_${PN} += "${datadir}"

