#! /bin/sh

# PATH should only include /usr/* if it runs after the mountnfs.sh script
PATH=/sbin:/usr/sbin:/bin:/usr/bin

DESC="Start broker server for voice"
NAME="broker"
DAEMON=/usr/share/broker-1.0/broker
DAEMON_ARGS=""
PIDFILE=/var/run/$NAME.pid
. /etc/init.d/functions || exit 1

# Exit if the package is not installed
[ -x "$DAEMON" ] || exit 0

# Read configuration variable file if it is present
[ -r /etc/default/$NAME ] && . /etc/default/$NAME

#
# Function that starts the daemon/service
#
do_start() {
    local status pid
    status=0
    pid=`pidofproc $NAME` || status=$?
    case $status in
        0)
            echo "$DESC already running ($pid)."
            exit 1
            ;;
        *)
            echo "Starting $DESC ..."
            start-stop-daemon --background --name $NAME --start --quiet --chuid root --exec $DAEMON -- ${DAEMON_ARGS}
            ;;
    esac
}

#
# Function that stops the daemon/service
#
do_stop() {
    local pid status
    status=0
    pid=`pidofproc $NAME` || status=$?
    case $status in
        0)
            start-stop-daemon --name $NAME --stop --quiet
            ;;
        *)
            echo "$DESC is not running; none killed." >&2
            ;;
    esac
    return $status
}

#
# Function that shows the daemon/service status
#
status_of_proc () {
    local pid status
    status=0
    pid=`pidofproc $NAME` || status=$?
    case $status in
        0)
            echo "$DESC is running ($pid)."
            exit 0
            ;;
        *)
            echo "$DESC is not running." >&2
            exit $status
            ;;
    esac
}

case "$1" in
    start)
        do_start
        ;;
    stop)
        do_stop || exit $?
        ;;
    status)
        status_of_proc
        ;;
    restart)
        do_stop
        do_start
        ;;
    try-restart|force-reload)
        do_stop && do_start
        ;;
    *)
        echo "Usage: $0 {start|stop|status|restart|try-restart|force-reload}" >&2
        exit 3
        ;;
esac
