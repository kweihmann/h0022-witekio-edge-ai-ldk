# Copyright (C) 2019 Witekio
# Released under the MIT license (see COPYING.MIT for the terms)
DESCRIPTION = "TensorFlow Lite and Qt/QML: face recognition example"
LICENSE = "GPLv3.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/LGPL-3.0-only;md5=bfccfe952269fff2b407dd11f2f3083b"

S = "${WORKDIR}/git"
SRC_URI = "git://git@bitbucket.org/adeneo-embedded/b2220-avnet-edge-ai.git;branch=yocto/dev;protocol=ssh \
	   file://qt-tflite.service \
	   file://qt-tflite.initd \
"
SRCREV = "eca2905e841ab15e2158fcee549a52aa6e942486"

DEPENDS = "qtquickcontrols2 \
           qtmultimedia \
	       qtwebsockets \
           qttools-native \
           tensorflow-lite \
           "

require recipes-qt/qt5/qt5.inc

CXXFLAGS_remove = "-O2"

do_install() {
	install -d ${D}${datadir}/${P}
	install -m 0755 ${B}/WitekioTFLite ${D}${datadir}/${P}/qt-tflite
	cp ${S}/*.qml ${D}${datadir}/${P}
	cp -R --no-dereference --preserve=mode,links ${S}/assets ${D}${datadir}/${P}

# Systemd service
	if ${@bb.utils.contains('DISTRO_FEATURES','systemd','true','false',d)}; then

		install -d ${D}${systemd_unitdir}
		install -m 0644 ${WORKDIR}/qt-tflite.service ${D}${systemd_unitdir}
	fi
# Sysvinit script
	if ${@bb.utils.contains('DISTRO_FEATURES', 'sysvinit', 'true', 'false', d)}; then
		install -Dm 0755 ${WORKDIR}/qt-tflite.initd ${D}${sysconfdir}/init.d/qt-tflite
	fi
}

FILES_${PN}-dbg += "${datadir}/${P}/.debug"
FILES_${PN} += "${datadir}"

RDEPENDS_${PN} = "qtquickcontrols2-qmlplugins \
                  qtgraphicaleffects-qmlplugins"
