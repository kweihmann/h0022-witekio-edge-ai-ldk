# Copyright (C) 2019 Witekio
# Released under the MIT license (see COPYING.MIT for the terms)
DESCRIPTION = "Azure cloud sync app"
LICENSE = "GPLv3.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/LGPL-3.0-only;md5=bfccfe952269fff2b407dd11f2f3083b"

S = "${WORKDIR}/git/demo-device"
SRC_URI = "gitsm://git@bitbucket.org/adeneo-embedded/b2207-r-d-avnet-iot-connect.git;branch=master;protocol=ssh \
	   file://cloud.initd \
"

SRCREV = "${AUTOREV}"

DEPENDS = "azure-iot-sdk-c"

inherit pkgconfig cmake
EXTRA_OECMAKE += "-DYOCTO_AZURE_INC=${STAGING_DIR_TARGET}${includedir}/azureiot"

inherit systemd update-rc.d
INITSCRIPT_NAME = "cloud"
INITSCRIPT_PARAMS = "start 99 5 2 . stop 20 0 ."

do_install() {
	install -d ${D}${datadir}/${P}
	install -m 0755 ${B}/demo_device ${D}${datadir}/${P}/cloud
	
	install -Dm 0755 ${WORKDIR}/cloud.initd ${D}${sysconfdir}/init.d/cloud
	
	install -d ${D}${datadir}/${P}/cert
	install -m 664 ${WORKDIR}/git/demo-device/cert/iotdevice.cert.pem ${D}${datadir}/${P}/cert/iotdevice.cert.pem
	install -m 664 ${WORKDIR}/git/demo-device/cert/iotdevice.key.pem ${D}${datadir}/${P}/cert/iotdevice.key.pem
}

FILES_${PN} += "${datadir}"
