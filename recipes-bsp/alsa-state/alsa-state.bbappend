# Append path for freescale layer to include custom alsa-state asound.conf
FILESEXTRAPATHS_prepend_mx6 := "${THISDIR}/files:"
FILESEXTRAPATHS_prepend_mx7 := "${THISDIR}/files:"
FILESEXTRAPATHS_prepend_mx8 := "${THISDIR}/files:"
FILESEXTRAPATHS_prepend_use-mainline-bsp := "${THISDIR}/files:"

PACKAGE_ARCH_mx6 = "${MACHINE_ARCH}"
PACKAGE_ARCH_mx7 = "${MACHINE_ARCH}"
PACKAGE_ARCH_mx8 = "${MACHINE_ARCH}"
PACKAGE_ARCH_use-mainline-bsp = "${MACHINE_ARCH}"
