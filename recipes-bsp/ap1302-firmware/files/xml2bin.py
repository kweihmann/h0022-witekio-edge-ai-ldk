#!/usr/bin/python3
# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright 2020 Laurent Pinchart <laurent.pinchart@ideasonboard.com>

import binascii
import struct
import sys
from xml.parsers import expat


class Parser(object):

    def __init__(self):
        self.__parser = expat.ParserCreate()
        self.__parser.StartElementHandler = self.__startElement
        self.__parser.EndElementHandler = self.__endElement
        self.__parser.CharacterDataHandler = self.__characterData

        self.__in_firmware = False
        self.__attrs = {}
        self.__data = ''

    def parse(self, filename):
        f = open(filename, 'rb')
        self.__parser.ParseFile(f)

    def data(self):
        return self.__data

    def __getitem__(self, name):
        return self.__attrs[name]

    def __startElement(self, name, attrs):
        if name == 'dump':
            self.__in_firmware = True
            self.__attrs = attrs

    def __endElement(self, name):
        if name == 'dump':
            self.__in_firmware = False

    def __characterData(self, data):
        if self.__in_firmware:
            self.__data += data


def main(argv):
    if len(argv) != 3:
        print(f'Usage: {argv[0]} input.xml output.bin')
        return 1

    parser = Parser()
    parser.parse(argv[1])

    fw = parser.data()
    fw = ''.join([c for c in fw if c in '0123456789abcdefABCDEF'])
    pll_init_size = int(parser['pll_init_size'])
    crc = int(parser['crc'], 16)

    out = open(argv[2], 'wb')
    out.write(struct.pack('<HH', pll_init_size, crc))
    out.write(binascii.a2b_hex(fw))


if __name__ == '__main__':
    sys.exit(main(sys.argv))