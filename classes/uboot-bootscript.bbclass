BOOTSCRIPT_SRC ??= ""
BOOTSCRIPT_FILE ??= "boot.scr"

DEPENDS += "u-boot-mkimage-native"

def get_src_uri(s, d):
    if s.strip(): return 'file://' + s
    return ''

SRC_URI_append = " ${@get_src_uri(d.getVar('BOOTSCRIPT_SRC'), d)}"

def map_bootscript_arch(a, d):
    import re

    if re.match('(i.86|athlon)$', a): return 'x86'
    elif re.match('aarch64$', a): return 'arm64'
    elif re.match('mips(|el)$', a): return 'mips'
    elif re.match('powerpc64$', a): return 'powerpc'
    return a

do_bootscript () {
    if [ -f ${WORKDIR}/${BOOTSCRIPT_SRC} ]; then
        uboot-mkimage -A ${@map_bootscript_arch(d.getVar('HOST_ARCH'), d)} -T script -O linux -d ${WORKDIR}/${BOOTSCRIPT_SRC} ${B}/${BOOTSCRIPT_FILE}
    fi
}

do_deploy_append () {
    if [ -r ${B}/${BOOTSCRIPT_FILE} ]; then
        install -m 0644 ${B}/${BOOTSCRIPT_FILE} ${DEPLOYDIR}
    fi
}

addtask bootscript before do_install after do_configure
