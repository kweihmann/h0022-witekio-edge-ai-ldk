# Copyright (C) 2020 Witekio
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-fsl/images/imx-image-multimedia.bb

inherit populate_sdk_qt5

CONFLICT_DISTRO_FeATURES = "directfb"

#DEPENDS_${PN} = "virtual/bootloader virtual/kernel"

ML_PKGS                   ?= ""
ML_STATICDEV              ?= ""
ML_PKGS_mx8                = "packagegroup-imx-ml"
ML_STATICDEV_mx8           = "tensorflow-lite-staticdev"

# Add opencv for i.MX GPU
OPENCV_PKGS       ?= ""
OPENCV_PKGS_imxgpu = " \
    opencv-apps \
    opencv-samples \
    python3-opencv \
"

IMAGE_INSTALL += " \
    crane oob voice setup cloud \
    ${OPENCV_PKGS} \
    ${ML_PKGS} \
    packagegroup-qt5-imx \
    qt-tflite \
    rsync \
    openssh-sftp-server \
"

TOOLCHAIN_TARGET_TASK += " \
    ${ML_STATICDEV} \
"

FEATURES_TO_REMOVE = " \
	package-management \
	splash \
"

IMAGE_FEATURES_remove ?= "${FEATURES_TO_REMOVE}"
